-- Adminer 4.8.1 MySQL 8.0.22 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `ml-users`;
CREATE TABLE `ml-users` (
  `ml-id` int NOT NULL AUTO_INCREMENT,
  `ml-username` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `ml-email` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `ml-password` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci NOT NULL,
  `ml-type` int NOT NULL,
  `ml-bio` text CHARACTER SET utf8 COLLATE utf8_czech_ci,
  `ml-status` varchar(255) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  `ml-created_at` datetime NOT NULL,
  `ml-birthyear` int NOT NULL,
  `ml-pfp` varchar(511) CHARACTER SET utf8 COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`ml-id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

INSERT INTO `ml-users` (`ml-id`, `ml-username`, `ml-email`, `ml-password`, `ml-type`, `ml-bio`, `ml-status`, `ml-created_at`, `ml-birthyear`, `ml-pfp`) VALUES
(1,	'insert',	'me@insrt.uk',	'6cf9bfb4f4da1863e885b66241a5edfa7a09118b',	7,	'Absolutely: based',	NULL,	'2021-06-14 00:01:39',	2003,	'https://autumn.revolt.chat/avatars/6rgg372gI2LrxCUx0CiA2R1Qs6eTtmC-2NpMq1Xa_3'),
(2,	'FatalErrorCoded',	'me@fatalerrorcoded.eu',	'ded7e22776f37ed2239982ecb75de1ab161784f9',	7,	'rtc engineer and an absolute idiot',	'ryzen 5800x moment',	'2021-06-14 00:02:57',	2003,	'https://autumn.revolt.chat/avatars/Cpf4Qio8QaWRheSgPhQipGe5WtI7mS1uMhcQlkSKUq'),
(3,	'skybird2333',	'chingchong@163.com',	'e575d96ae60721e08175d1c58ccbbb81e6cfa2d4',	3,	NULL,	'!thonk help',	'2021-06-14 00:05:20',	2002,	'https://autumn.revolt.chat/avatars/ZPkJAnVDI_cM7iosMRDrlIEUupwK0NEV6X6EbUwwDK'),
(4,	'jan4',	'janus@seznam.cz',	'9491086fa7ae91482bd3c8954aa737474f17fba9',	11,	'Developer of the current Revolt & D*scord bridge, Revolt.Net and Taco',	'https://revolt.gay',	'2021-06-14 00:06:38',	2005,	'https://autumn.revolt.chat/avatars/UR7oy-DXVmEQyl37jS1Bv6VyZpK4qCxlgUMaaFriGU'),
(5,	'Ebuchkius',	'bucharsky@seznam.cz',	'fa36fd23064fccd84c803355438bbb8a80aa61a3',	1,	NULL,	NULL,	'2021-06-14 00:07:39',	2002,	'https://autumn.revolt.chat/avatars/YP9u0q9P354WASR332cymGc8OvXWwEhAil4CPoKWsV'),
(6,	'chabr33',	'tomasd@seznam.cz',	'289fd37ad85aa40347045ac62c117bbff8c65b0a',	1,	NULL,	NULL,	'2021-06-14 00:08:30',	2001,	'https://autumn.revolt.chat/avatars/JMBLS3F8KEPJseusdBgnvDzeSmYBE1LYExyGif-7Xr'),
(7,	'Wait_What_',	'waitwhat@tutanota.com',	'4a11232152ea6003d64068b44b6d1cb2635c095c',	11,	'If you are reading this, I have successfully injected a script element into the site\'s HTML.',	'order a revolt keychain NOW',	'2021-06-14 00:12:18',	2004,	'https://autumn.revolt.chat/avatars/Phh5BkJLAJinbbzyvHedQRipX-NZMcZqk6MbfP4gaH'),
(8,	'Adam_K',	'adam.kubes2002@gmail.com',	'ad3aa3ce2eb4c32ceb1d53028c0d36bb677e189b',	1,	NULL,	NULL,	'2021-06-14 00:13:18',	2003,	'https://autumn.revolt.chat/avatars/bDLMa1eSvgdCoIDkWZvMfszIlXRNaQHkCOEIOv-aEp'),
(9,	'LetsSuicide',	'jan.hostalek@email.cz',	'4b0a962f6404209a9fd8da5bbc7391cdb3d7eba7',	1,	'Deja Vu I\'ve just been in this place before',	NULL,	'2021-06-14 00:15:19',	2004,	'https://autumn.revolt.chat/avatars/VZFXDGROfgK-Hr84WwNODXMAxIRUJfuMD19YSttx0w'),
(10,	'nizune',	'rene.vinter@outlook.com',	'3e1816cdeb72b3c59d897d1756a6c46e5b62eaf5',	7,	'GrApHicS DeSiGn iS My pAsSiOn',	NULL,	'2021-06-14 00:16:57',	2002,	'https://autumn.revolt.chat/avatars/gh6d_E4vW4v0xofbKxpsPDPT4DT-3ETSRhXUMIN5ko');

-- 2021-06-14 00:22:48
